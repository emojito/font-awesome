import json

from importlib import resources
from packaging.version import parse as version


# Import metadata
metadata = json.loads(resources.read_text(__package__, 'metadata.json'))

# Set what the latest version of all icons is
latest_version = max(map(version, [v for _, d in metadata.items() for v in d.get('changes')]))